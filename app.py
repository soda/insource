# app.py
from flask import Flask, render_template, request
import requests

app = Flask(__name__)

WIKI_API_URL = "https://en.wikipedia.org/w/api.php"

def search_wikipedia(query):
    params = {
        "action": "query",
        "list": "search",
        "srsearch": f"insource:{query}",
        "format": "json"
    }
    response = requests.get(WIKI_API_URL, params=params)
    data = response.json()
    return data['query']['search']

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/search', methods=['POST'])
def search():
    query = request.form['query']
    results = search_wikipedia(query)
    return render_template('results.html', results=results)

if __name__ == '__main__':
    app.run(debug=True)
